var app = require('./config/server');

var rotaNoticias = require('./app/routes/noticias')(app);

var rotaHome = require('./app/routes/home')(app);

var rotaFormInclusaoNoticia = require('./app/routes/form_inclusao_noticia')(app);

var rotaModa = require('./app/routes/moda')(app);

var rotaEsportes = require('./app/routes/esportes')(app);

var rotaTecnologia = require('./app/routes/tecnologia')(app);

var rotaCuriosidades = require('./app/routes/curiosidades')(app);

app.listen(3000, function(){
    console.log("Servidor ON");
});